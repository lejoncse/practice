<html>
    <!--boolval-->
    <!--boolval — Get the boolean value of a variable-->
   <?php
//echo '0:        '.(boolval(0) ? 'true' : 'false')."<br/>";
////echo '42:       '.(boolval(42) ? 'true' : 'false')."\n";
////echo '0.0:      '.(boolval(0.0) ? 'true' : 'false')."\n";
////echo '4.2:      '.(boolval(4.2) ? 'true' : 'false')."\n";
////echo '"":       '.(boolval("") ? 'true' : 'false')."\n";
////echo '"string": '.(boolval("string") ? 'true' : 'false')."\n";
////echo '"0":      '.(boolval("0") ? 'true' : 'false')."\n";
////echo '"1":      '.(boolval("1") ? 'true' : 'false')."\n";
////echo '[1, 2]:   '.(boolval([1, 2]) ? 'true' : 'false')."\n";
////echo '[]:       '.(boolval([]) ? 'true' : 'false')."\n";
////echo 'stdClass: '.(boolval(new stdClass) ? 'true' : 'false')."\n";
?>
  <!--empty-->  
  <!--empty — Determine whether a variable is empty-->
    <?php
$var = 0;

// Evaluates to true because $var is empty
if (empty($var)) {
    echo '$var is either 0, empty, or not set at all';
}

// Evaluates as true because $var is set
if (isset($var)) {
    echo '$var is set even though it is empty';
}
?>
   <br/> 
    <?php
$var = 1;

// Evaluates to true because $var is empty
if (empty($var)) {
    echo '$var is either 0, empty, or not set at all';
}

// Evaluates as true because $var is set
if (isset($var)) {
    echo '$var is set even though it is empty';
}
?>
   <br/>
<!--   floatval
   floatval — Get float value of a variable-->
    <?php
$var = '122.34343The';
$float_value_of_var = floatval($var);
echo $float_value_of_var; 
?>
<br/>
<?php
$var = 'The122.34343';
$float_value_of_var = floatval($var);
echo $float_value_of_var; 
?>
<br/>
<!--gettype — Get the type of a variable-->
<?php

//$data = array(1, 2.54145, NULL, new stdClass, 'foo');
//
//foreach ($data as $value) {
//    echo gettype($value), "\n";
//}
$data = array(1, 2.5454, NULL, new stdClass, 'khan');
foreach ($data as$value){
    echo gettype ($value),"\n";
}
?>
<br/>
<!--intval — Get the integer value of a variable-->
<?php
echo intval (100);echo"<br/>";
echo intval(42); echo"<br/>";                 
echo intval(4.2); echo"<br/>";                    
echo intval('42'); echo"<br/>";                   
echo intval('+42'); echo"<br/>";                 
echo intval('-42');  echo"<br/>";                 
echo intval(042);   echo"<br/>";                 
echo intval('042'); echo"<br/>";                  
echo intval(1e10);  echo"<br/>";                  
echo intval('1e10'); echo"<br/>";                 
echo intval(0x1A);   echo"<br/>";                
echo intval(42000000);echo"<br/>";                
echo intval(420000000000000000000);  echo"<br/>"; 
echo intval('420000000000000000000');echo"<br/>"; 
echo intval(42, 85);   echo"<br/>";               
echo intval('48', 18); echo"<br/>";               
echo intval(array());  echo"<br/>";               
echo intval(array('foo', 'bar'));     
?>
<br/>
<!--unset — Unset a given variable-->
<?php
function foo() 
{
    unset($GLOBALS['bar']);
}

$bar = "something";
foo();
?>
</html>